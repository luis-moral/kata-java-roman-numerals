public class RomanNumerals {

    private enum RomanNumber {
        M(1000),
        CM(900),
        D(500),
        CD(400),
        C(100),
        XC(90),
        L(50),
        XL(40),
        X(10),
        IX(9),
        V(5),
        IV(4),
        I(1);

        private int decimalValue;

        RomanNumber(int decimalValue) {
            this.decimalValue = decimalValue;
        }

        public int getDecimalValue() {
            return decimalValue;
        }
    }

    public static String toRomanNumber(int number) {
        String result = "";

        while (number > 0) {
            for (int i = 0; i < RomanNumber.values().length; i++) {
                RomanNumber romanNumber = RomanNumber.values()[i];

                if (number >= romanNumber.getDecimalValue()) {
                    number = number - romanNumber.getDecimalValue();
                    result = result.concat(romanNumber.toString());
                    break;
                }
            }
        }

        return result;
    }
}
