import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class RomanNumeralsShould {

    @Test
    public void convert_from_1_to_3() {
        assertThat(RomanNumerals.toRomanNumber(1)).isEqualTo("I");
        assertThat(RomanNumerals.toRomanNumber(2)).isEqualTo("II");
        assertThat(RomanNumerals.toRomanNumber(3)).isEqualTo("III");
    }

    @Test
    public void convert_from_5_to_8() {
        assertThat(RomanNumerals.toRomanNumber(5)).isEqualTo("V");
        assertThat(RomanNumerals.toRomanNumber(6)).isEqualTo("VI");
        assertThat(RomanNumerals.toRomanNumber(7)).isEqualTo("VII");
        assertThat(RomanNumerals.toRomanNumber(8)).isEqualTo("VIII");
    }

    @Test
    public void convert_4_and_9() {
        assertThat(RomanNumerals.toRomanNumber(4)).isEqualTo("IV");
        assertThat(RomanNumerals.toRomanNumber(9)).isEqualTo("IX");
    }

    @Test
    public void convert_1_and_multiples_of_10() {
        assertThat(RomanNumerals.toRomanNumber(1)).isEqualTo("I");
        assertThat(RomanNumerals.toRomanNumber(10)).isEqualTo("X");
        assertThat(RomanNumerals.toRomanNumber(100)).isEqualTo("C");
        assertThat(RomanNumerals.toRomanNumber(1000)).isEqualTo("M");
    }

    @Test
    public void convert_2_and_multiples_of_20() {
        assertThat(RomanNumerals.toRomanNumber(2)).isEqualTo("II");
        assertThat(RomanNumerals.toRomanNumber(20)).isEqualTo("XX");
        assertThat(RomanNumerals.toRomanNumber(200)).isEqualTo("CC");
        assertThat(RomanNumerals.toRomanNumber(2000)).isEqualTo("MM");
    }

    @Test
    public void convert_3_and_multiples_of_30() {
        assertThat(RomanNumerals.toRomanNumber(3)).isEqualTo("III");
        assertThat(RomanNumerals.toRomanNumber(30)).isEqualTo("XXX");
        assertThat(RomanNumerals.toRomanNumber(300)).isEqualTo("CCC");
        assertThat(RomanNumerals.toRomanNumber(3000)).isEqualTo("MMM");
    }

    @Test
    public void convert_4_and_multiples_of_40() {
        assertThat(RomanNumerals.toRomanNumber(4)).isEqualTo("IV");
        assertThat(RomanNumerals.toRomanNumber(40)).isEqualTo("XL");
        assertThat(RomanNumerals.toRomanNumber(400)).isEqualTo("CD");
        assertThat(RomanNumerals.toRomanNumber(4000)).isEqualTo("MMMM");
    }

    @Test
    public void convert_5_and_multiples_of_50() {
        assertThat(RomanNumerals.toRomanNumber(5)).isEqualTo("V");
        assertThat(RomanNumerals.toRomanNumber(50)).isEqualTo("L");
        assertThat(RomanNumerals.toRomanNumber(500)).isEqualTo("D");
    }

    @Test
    public void convert_6_and_multiples_of_60() {
        assertThat(RomanNumerals.toRomanNumber(6)).isEqualTo("VI");
        assertThat(RomanNumerals.toRomanNumber(60)).isEqualTo("LX");
        assertThat(RomanNumerals.toRomanNumber(600)).isEqualTo("DC");
    }

    @Test
    public void convert_7_and_multiples_of_70() {
        assertThat(RomanNumerals.toRomanNumber(7)).isEqualTo("VII");
        assertThat(RomanNumerals.toRomanNumber(70)).isEqualTo("LXX");
        assertThat(RomanNumerals.toRomanNumber(700)).isEqualTo("DCC");
    }

    @Test
    public void convert_8_and_multiples_of_80() {
        assertThat(RomanNumerals.toRomanNumber(8)).isEqualTo("VIII");
        assertThat(RomanNumerals.toRomanNumber(80)).isEqualTo("LXXX");
        assertThat(RomanNumerals.toRomanNumber(800)).isEqualTo("DCCC");
    }

    @Test
    public void convert_9_and_multiples_of_90() {
        assertThat(RomanNumerals.toRomanNumber(9)).isEqualTo("IX");
        assertThat(RomanNumerals.toRomanNumber(90)).isEqualTo("XC");
        assertThat(RomanNumerals.toRomanNumber(900)).isEqualTo("CM");
    }

    @Test
    public void convert_any_valid_number() {
        assertThat(RomanNumerals.toRomanNumber(1990)).isEqualTo("MCMXC");
        assertThat(RomanNumerals.toRomanNumber(2008)).isEqualTo("MMVIII");
        assertThat(RomanNumerals.toRomanNumber(99)).isEqualTo("XCIX");
        assertThat(RomanNumerals.toRomanNumber(47)).isEqualTo("XLVII");
    }
}
